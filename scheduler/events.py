"""
Events are represented by a :py:class:`.Event` instance.

"""
import dataclasses
import datetime
import typing


@dataclasses.dataclass
class Event:
    """
    An event specified by an ingest source. This class is a `dataclass
    <https://docs.python.org/3/library/dataclasses.html>`_ and supports all the dataclass methods.

    """
    #: A unique id which is stable when other event data changes.
    id: str

    #: Flag which indicates that this event which was previously scheduled should now be cancelled.
    cancelled: bool

    #: A list of crsids for "lecturers". (I.e. people who have had to opt-in to this recording.)
    lecturer_crsids: typing.Sequence[str]

    #: A descriptive title for the event.
    title: str

    #: The date and time at which the event starts.
    start_at: datetime.datetime

    #: The duration of the event.
    duration: datetime.timedelta

    #: A list of URLs pointing to related courses in the VLE.
    vle_urls: typing.Sequence[str]

    #: A unique id which specifies the sequence of lectures which this event is part of.
    sequence_id: str

    #: The index within the sequence at which this event should sit.
    sequence_index: int

    #: A unique id which specifies the physical location where the event takes place.
    location_id: str

    #: A unique id which specifies which series this event is a part of. Note that the "series" and
    #: "sequence" fields are only weakly related. The "series" field relates to how events should
    #: grouped when presented to the user and the "sequence" field describes how they are grouped
    #: from an organisational PoV.
    #:
    #: If one has no information to the contrary, one may use the convention of having one series
    #: for each VLE course.
    series_id: str


@dataclasses.dataclass
class EventMetadata:
    """
    Metadata added to an event by the scheduler.

    """
    #: A unique id which specifies which lecture capture agent has been given responsibility for
    #: recording the event.
    agent_id: str

    #: If this flag is set, any existing event should be removed if it is scheduled for recording.
    is_revoked: bool
