"""
Ingest of events from Google Sheets.

"""
import asyncio
import dataclasses
import datetime
import logging
import typing

import dateutil.parser
import dateutil.tz
import googleapiclient.discovery
import gspread_asyncio
import httplib2shim  # more modern httplib2 shim
from oauth2client.service_account import ServiceAccountCredentials

from .config import ConfigurationDataclassMixin
from .events import Event
from .state import StateUpdate

LOG = logging.getLogger(__name__)

# Scopes required to access Google spreadsheets
SCOPES = [
    'https://spreadsheets.google.com/feeds',
    'https://www.googleapis.com/auth/drive'
]

# Expected column headings
HEADINGS = [
    'id', 'cancelled', 'lecturer_crsids', 'start_at', 'duration_minutes', 'title', 'vle_urls',
    'sequence_id', 'sequence_index', 'location_id', 'series_id'
]

#: Default delay (in seconds) between polls to the Drive API. Changes to Google Sheets are slow to
#: manifest themselves in the modification time so there is no advantage in this being any smaller
#: than around 5 minutes.
DEFAULT_POLL_DELAY = 60*5


class ParseError(RuntimeError):
    """An exception raised if the parsed Google sheet has an invalid format."""


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    """
    Configuration for the Google Sheets ingest.

    """
    # Path to JSON service account credentials
    service_account_credentials_path: str

    # List of keys representing spreadsheets to read
    keys: typing.List[str]

    # How long to wait between polling Google API for changes
    poll_delay_seconds: typing.Union[int, float] = DEFAULT_POLL_DELAY


async def loop(*, queue, configuration):
    """
    Coroutine which notices changes to watched Google spreadsheets and pushes lists of new
    :py:class:`scheduler.events.Event` instances to *queue*.

    :param queue: queue which receives lists of new events
    :type queue: asyncio.Queue

    """
    # Load credentials
    LOG.info('Loading credentials from: %s', configuration.service_account_credentials_path)
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        configuration.service_account_credentials_path, SCOPES)

    # The Google discovery API client prints warnings about not being able to use a file cache
    # which is benign but includes an exception traceback which can confuse some log parsers. Set
    # the log level for that module to ERROR to silence them.
    logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.ERROR)

    # Create a Google drive client
    drive_service = await _build_drive_service(credentials)

    # Now we've built the service, reset the log level to NOTSET. Unfortunately, there's not an
    # official way of determining the actual log level of the logger to reset it to exactly what it
    # was before.
    logging.getLogger('googleapiclient.discovery_cache').setLevel(logging.NOTSET)

    # Ensure sequence of keys is a set
    keys = set(configuration.keys)

    # The following loop asks the Google Drive API which sheets have changed since the latest
    # modification time we're aware of. The new latest modification time is remembered for the next
    # iteration of the loop and any sheets which have changed which we're interested in are passed
    # to _fetch_and_parse_sheets.
    #
    # There appears to be some latency between Google sheets being updated and it being reflected
    # in the modification time. Delays of up to 5 minutes have been observed. Consequently it's
    # counter-productive to poll too frequently in this loop.
    latest_modified_time = None
    while True:
        # Form a query for the spreadsheets we're interested in.
        query = "mimeType = 'application/vnd.google-apps.spreadsheet' and trashed = false"
        if latest_modified_time is not None:
            LOG.info('Looking for modified time after %s', latest_modified_time)
            query = query + f" and modifiedTime > '{latest_modified_time}'"
        else:
            LOG.info('Fetching all sheets')

        # Get file metadata from Drive API.
        files = await _drive_list_files(
            drive_service, query=query, extra_fields=[
                'files/modifiedTime', 'files/id'
            ]
        )

        # If we found at least one file, update the cached latest modified time to the latest value
        # we get from the API response so that next time around we only get the files which have
        # changed since.
        if len(files) > 0:
            latest_modified_time = max(
                files, key=lambda metadata: dateutil.parser.isoparse(metadata['modifiedTime'])
            )['modifiedTime']
            LOG.info('New latest modified time: %r', latest_modified_time)

        # Filter returned files to those we're interested in.
        keys_to_process = [
            metadata['id'] for metadata in files if metadata['id'] in keys
        ]

        # Process sheets we're interested in. This co-routine logs exceptions but does not re-raise
        # them.
        await _fetch_and_parse_sheets(credentials, keys_to_process, queue)

        # Wait for the next poll time.
        LOG.info('Sleeping for %s seconds', configuration.poll_delay_seconds)
        await asyncio.sleep(configuration.poll_delay_seconds)


async def _fetch_and_parse_sheets(credentials, keys, queue):
    """
    Co-routine which process all passed sheet keys. Exceptions from processing are captured and
    logged but are not re-raised to the caller.

    """
    # Create a Google Sheets API client. We have to do this here because we will likely go for long
    # periods before we load data from Google sheets and the authorisation token used by the client
    # has limited lifetime.
    sheets = await gspread_asyncio.AsyncioGspreadClientManager(lambda: credentials).authorize()

    results = await asyncio.gather(*[_fetch_and_parse_sheet(sheets, key, queue) for key in keys])
    for result, key in zip(results, keys):
        if isinstance(result, Exception):
            LOG.error('sheet %s: error: %s', key, result, exc_info=result)


async def _fetch_and_parse_sheet(sheets, key, queue):
    """
    Co-routine which processes an individual sheet. Any exceptions from processing the sheet are
    raised back to the caller. Exceptions for individual rows are logged but are note re-raised.

    """
    sheet = await sheets.open_by_key(key)

    LOG.info('sheet: %s: getting rows for first worksheet', key)
    worksheet = await sheet.get_worksheet(0)
    rows = await worksheet.get_all_values()
    LOG.info('sheet: %s: got %s row(s)', key, len(rows))
    await queue.put(StateUpdate(events=parse_sheet(rows)))


def parse_sheet(rows):
    """
    Parse the rows of a sheet.

    :param rows: sequence of sequences of strings representing the rows of the sheet

    """
    if len(rows) < 1:
        raise ParseError('There must be at least one heading row')

    headings, data = rows[0], rows[1:]
    if any(expected != got for expected, got in zip(HEADINGS, headings)):
        raise ParseError(f'Heading row mismatch. Expected "{HEADINGS}", got "{headings}"')

    # Iterate over each data row, converting it into a dictionary and passing to parse_event
    ingest_events = []
    for datum in data:
        datum_dict = {key: value for key, value in zip(headings, datum)}
        event_id = datum_dict.get('id', '{unknown}').strip()
        try:
            event = parse_event(datum_dict)
        except ParseError as e:
            LOG.warning('Skipping event %s which failed to parse: %s', event_id, e, exc_info=e)
        else:
            ingest_events.append(event)

    return ingest_events


def parse_event(event_dict):
    """
    Parse a single event row. Returns a tuple (cancelled, event). If cancelled is True, event is
    only guaranteed to contain the event id.

    :param event_dict: a dictionary representation of a row
    :raises: :py:class:`.ParseError` if the event cannot be parsed.

    """
    # We use _get_str for most fields but the id needs to be strictly checked.
    event_id = event_dict.get('id')
    if event_id is None or not isinstance(event_id, str) or event_id.strip() == '':
        raise ParseError(f'invalid event id: "{event_id}"')

    cancelled = _get_str(event_dict, 'cancelled')
    if cancelled == 'Y':
        is_cancelled = True
    elif cancelled == '' or cancelled == 'N':
        is_cancelled = False
    else:
        raise ParseError(f'cancelled should be empty, "Y" or "N". It was: "{cancelled}"')

    # Strip any leading/trailing whitespace from id.
    event_id = event_id.strip()

    # Parse lecturer crsids list
    lecturer_crsids_list = _get_str(event_dict, 'lecturer_crsids')
    lecturer_crsids = [
        crsid.strip() for crsid in lecturer_crsids_list.split(',')
        if crsid.strip() != ''
    ]

    # Parse starting date and duration
    try:
        start_at = _iso8601_parse(_get_str(event_dict, 'start_at'))
    except ValueError:
        raise ParseError(
            'Event has invalid start time: %r' % _get_str(event_dict, 'start_at'))

    try:
        duration = datetime.timedelta(minutes=int(_get_str(event_dict, 'duration_minutes')))
    except ValueError:
        raise ParseError(
            'Event has invalid duration: %r' % _get_str(event_dict, 'duration_minutes'))

    if duration.total_seconds() <= 0:
        raise ParseError(f'Event has invalid duration: {duration}')

    sequence_id = _get_str(event_dict, 'sequence_id')
    if sequence_id == '':
        raise ParseError('empty sequence id')
    try:
        sequence_index = int(_get_str(event_dict, 'sequence_index'))
    except ValueError:
        raise ParseError('invalid sequence index: "%s"' % (_get_str(event_dict, 'sequence_index')))
    if sequence_index < 0:
        raise ParseError(f'invalid sequence index: {sequence_index}')

    location_id = _get_str(event_dict, 'location_id')
    if location_id == '':
        raise ParseError('empty location id')

    series_id = _get_str(event_dict, 'series_id')
    if series_id == '':
        raise ParseError('empty series id')

    # Parse VLE URL list
    vle_urls_list = _get_str(event_dict, 'vle_urls')
    vle_urls = [
        vle_url.strip() for vle_url in vle_urls_list.split(',')
        if vle_url.strip() != ''
    ]

    return Event(
        id=event_id,
        cancelled=is_cancelled,
        lecturer_crsids=lecturer_crsids,
        title=_get_str(event_dict, 'title'),
        start_at=start_at, duration=duration,
        vle_urls=vle_urls,
        sequence_id=sequence_id, sequence_index=sequence_index,
        location_id=location_id,
        series_id=series_id
    )


def _get_str(d, key, default=None, *, strip=True):
    """
    Return a key's value from dictionary *d* as a string. If *strip* is True, also strip leading
    and trailing whitespace. If the key does not exist, use *default* as the value

    >>> _get_str({'foo': 34}, 'foo')
    '34'
    >>> _get_str({'foo': '  bar '}, 'foo')
    'bar'
    >>> _get_str({'foo': None}, 'foo')
    'None'
    >>> _get_str({'foo': '  bar '}, 'foo', strip=False)
    '  bar '
    >>> _get_str({'foo': 'bar'}, 'buzz')
    'None'
    >>> _get_str({'foo': 'bar'}, 'buzz', default=' bar  ')
    'bar'
    >>> _get_str({'foo': 'bar'}, 'buzz', ' bar  ')
    'bar'

    """
    value = str(d.get(key, default))
    return value if not strip else value.strip()


def _iso8601_parse(datetime_str):
    """
    Parse an ISO8601 formatted date string to a datetime object. The returned object is always in
    the UTC timezone and appropriate timezone conversion is applied from the input string.

    >>> _iso8601_parse('2006-01-02T15:04:05-07:00')
    datetime.datetime(2006, 1, 2, 22, 4, 5, tzinfo=tzutc())
    >>> _iso8601_parse('2006-01-02')
    datetime.datetime(2006, 1, 2, 0, 0, tzinfo=tzutc())
    >>> _iso8601_parse('2006-01-02T-07')
    datetime.datetime(2006, 1, 2, 7, 0, tzinfo=tzutc())
    >>> _iso8601_parse('not a date')
    Traceback (most recent call last):
        ...
    ValueError: ...
    >>> _iso8601_parse('')
    Traceback (most recent call last):
        ...
    ValueError: ...

    """
    return dateutil.parser.isoparse(datetime_str).astimezone(dateutil.tz.tzutc())


async def _build_drive_service(credentials):
    """
    Co-routine which builds a Google Drive API service using the Google API client libraries. Runs
    the code within the default asyncio executor.

    """
    def build_service():
        http = httplib2shim.Http()
        return googleapiclient.discovery.build('drive', 'v3', http=credentials.authorize(http))
    return await asyncio.get_running_loop().run_in_executor(None, build_service)


async def _drive_list_files(drive_service, query=None, extra_fields=None):
    """
    Co-routine which uses the Google drive API to get a list of files. Automatically handles paging
    as necessary to retrieve a full list.

    """
    # Get the current asyncio event loop so that we can call the drive API in the loop's default
    # thread pool.
    loop = asyncio.get_running_loop()

    # List of file metadata resources to return
    files = []

    # List of fields in metadata to return
    fields = ','.join(extra_fields) if extra_fields is not None else None

    # Loop while we wait for nextPageToken to be "none"
    page_token = None
    while True:
        list_response = await loop.run_in_executor(
            None, drive_service.files().list(
                corpora='user,allTeamDrives', supportsTeamDrives=True, fields=fields,
                includeTeamDriveItems=True, pageToken=page_token, q=query
            ).execute
        )

        # Add returned files to response list
        files.extend(list_response.get('files', []))

        # Get the token for the next page
        page_token = list_response.get('nextPageToken')
        if page_token is None:
            break

    return files
