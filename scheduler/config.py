"""
Utilities for parsing configuration files.

"""
import dataclasses
import logging
import os

import yaml

LOG = logging.getLogger(__name__)


class ConfigurationError(RuntimeError):
    pass


class ConfigurationNotFound(ConfigurationError):
    def __init__(self):
        return super().__init__('Could not find any configuration file')


def load_configuration(location=None):
    """
    Load configuration and return a :py:class:`Configuration` instance. Pass a non-None location to
    override the default search path.

    :raises: ConfigurationError if the configuration could not be loaded.

    """
    if location is not None:
        paths = [location]
    else:
        if 'SCHEDULER_CONFIGURATION' in os.environ:
            paths = [os.environ['SCHEDULER_CONFIGURATION']]
        else:
            paths = []
        paths.extend([
            os.path.join(os.getcwd(), '.scheduler/configuration.yaml'),
            os.path.expanduser('~/.scheduler/configuration.yaml'),
            '/etc/scheduler/configuration.yaml'
        ])

    valid_paths = [path for path in paths if os.path.isfile(path)]

    if len(valid_paths) == 0:
        LOG.error('Could not find configuration file. Tried:')
        for path in paths:
            LOG.error('"%s"', path)
        raise ConfigurationNotFound()

    with open(valid_paths[0]) as f:
        return yaml.safe_load(f)


class ConfigurationDataclassMixin:
    """
    Mixin class for dataclass which adds a "from_dict" member which will construct an instance from
    a dictionary. Fields which have no default value become required fields.

    """

    @classmethod
    def from_dict(cls, dict_):
        """
        Construct an instance from a dict.

        """
        field_names = {field.name for field in dataclasses.fields(cls)}
        required_field_names = {
            field.name for field in dataclasses.fields(cls)
            if (
                field.default is dataclasses.MISSING
                and field.default_factory is dataclasses.MISSING
            )
        }

        for key in dict_.keys():
            if key not in field_names:
                raise ValueError(f'Unknown configuration key: {key}')

        for key in required_field_names:
            if key not in dict_:
                raise ValueError(f'{key}: required field not set')

        return cls(**dict_)
