"""
Schedule Lecture Capture Recordings

Usage:
    scheduler (-h | --help)
    scheduler [(--quiet | --verbose)] [--configuration=PATH]

Options:

    -h, --help                  Show a brief usage summary.
    --quiet                     Reduce logging verbosity.
    --verbose                   Increase logging verbosity.
    --configuration=PATH        Override location of configuration file (see below).

Configuration:

    Configuration of the tool is via a YAML document. The default location for the configuration is
    the first file which exists in the following locations:

    - The value of the SCHEDULER_CONFIGURATION environment variable
    - ./.scheduler/configuration.yaml
    - ~/.scheduler/configuration.yaml
    - /etc/scheduler/configuration.yaml

    The --configuration option may be used to override the search path.

"""
import logging
import sys

import docopt

from . import config, loop


LOG = logging.getLogger(__name__)


def main():
    opts = docopt.docopt(__doc__)

    if opts['--verbose']:
        logging.basicConfig(level=logging.DEBUG)
    elif opts['--quiet']:
        logging.basicConfig(level=logging.WARN)
    else:
        logging.basicConfig(level=logging.INFO)

    # Load the configuration
    try:
        configuration = config.load_configuration(location=opts.get('--configuration'))
    except config.ConfigurationError as e:
        LOG.error('Could not load configuration: %s', e)
        sys.exit(1)

    try:
        loop.run(configuration)
    except Exception as e:
        LOG.error('error: %s', e, exc_info=e)
        sys.exit(1)
