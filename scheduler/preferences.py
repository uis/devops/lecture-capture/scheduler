"""
Synchronisation of lecturer preferences

"""
import asyncio
import dataclasses
import logging
import typing
import urllib.parse as urlparse

import aiohttp

from .config import ConfigurationDataclassMixin
from .state import StateUpdate

LOG = logging.getLogger(__name__)

#: Default delay (in seconds) between polls to the Preferences API. There's not much point in
#: making this be very small since lecturer preferences are supposed to change infrequently.
DEFAULT_POLL_DELAY = 5*60


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    """
    Configuration for the preferences synchronisation.

    """
    #: Base URL of preferences webapp
    base_url: str

    #: How long to wait between polling API for changes
    poll_delay_seconds: typing.Union[int, float] = DEFAULT_POLL_DELAY


async def loop(*, queue, configuration):
    preferences_list_url = urlparse.urljoin(configuration.base_url, '/api/preferences/')

    while True:
        LOG.info('Querying latest preferences')

        # Get all pages of preferences results and place them in the preferences variable.
        preferences = await _fetch_all_preferences(preferences_list_url)

        # Turn lecturer preferences into opt-ins and post a state update.
        await queue.put(StateUpdate(opt_ins=[
            pref['user']['username'] for pref in preferences if pref['allow_capture']
        ]))

        # Wait for the next poll time.
        LOG.info('Sleeping for %s seconds', configuration.poll_delay_seconds)
        await asyncio.sleep(configuration.poll_delay_seconds)


async def _fetch_all_preferences(url):
    """
    Fetch all the preferences from the preferences list URL passed. All pages are merged into one
    list of results which is returned. Any errors will cause an exception to be raised.

    """
    preferences = []
    async with aiohttp.ClientSession() as session:
        while url is not None:
            async with session.get(url) as resp:
                if resp.status != 200:
                    raise RuntimeError(f'Unexpected status: {resp.status}')
                resp_body = await resp.json()
                preferences.extend(resp_body.get('results', []))
                url = resp_body.get('next')
    return preferences
