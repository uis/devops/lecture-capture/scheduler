import dataclasses
import datetime
import random
import unittest
import uuid

from .. import events
from .. import state


def _random_string(k=20):
    """
    Use random's RNG to generate a random string

    """
    return ''.join(random.choices('ABCDEFGHIJKLMNOPQRSTUVWXYZ', k=k))


# Create some random data. Use a fixed seed for reproducibility.
random.seed(0xdeadbeef)
FAKE_CRSIDS = ['abc1', 'def1', 'spqr2', 'xyz89']
FAKE_SEQUENCES = [_random_string() for _ in range(10)]
FAKE_LOCATIONS = [_random_string() for _ in range(10)]
FAKE_SERIES = [_random_string() for _ in range(10)]

# Mapping from location to agent
LOCATION_TO_AGENTS = {loc: _random_string() for loc in FAKE_LOCATIONS}


class StateTestCase(unittest.TestCase):
    def setUp(self):
        # Reset RNG seed to ensure results are reproducible
        random.seed(0xface1234)

        # Create a blank state
        self.state = state.State()

        # Populate the state with some valid events
        self._populate_state()

    def test_basic_functionality(self):
        """
        A state populated with events which should all be scheduled schedules all the events.

        """
        schedule = self.state.get_schedule()
        events = self.state.events

        # More than 0 events were ingested and they are all scheduled
        scheduled_event_ids = {e.id for e, _ in schedule}
        event_ids = set(events.keys())
        self.assertGreater(len(events), 0)
        self.assertEqual(scheduled_event_ids, event_ids)

        for event, metadata in schedule:
            self.assertEqual(metadata.agent_id, LOCATION_TO_AGENTS[event.location_id])

    def test_cancelled_events(self):
        """
        Cancelling an event removes it from the schedule.

        """
        scheduled_event_ids_before = self._get_scheduled_event_ids()
        new_event = self._update_random_event(cancelled=True)
        self.assertIn(new_event.id, scheduled_event_ids_before)
        self.assertIn(new_event.id, self._get_revoked_event_ids())

    def test_lack_of_opt_in(self):
        """
        Removing a lecturer opt-in removes it from the schedule.

        """
        test_crsid = 'testing12345'
        new_event = self._update_random_event(lecturer_crsids=[test_crsid])

        # An event without an opt-in from lecturer is not scheduled
        self.assertIn(new_event.id, self._get_revoked_event_ids())

        # Adding the opt-in schedules the event
        self.state.opt_ins |= {test_crsid}
        self.assertIn(new_event.id, self._get_scheduled_event_ids())

    def test_missing_agent(self):
        """
        Check that an event is scheduled only if there is a capture agent at that location.
        """
        location_id = 'the-moon'
        new_event = self._update_random_event(location_id=location_id)

        # An event whose location doesn't have a capture agent is not scheduled
        self.assertIn(new_event.id, self._get_revoked_event_ids())

        # Adding the capture agent mapping schedules the event
        self.state.location_to_agents.update({location_id: 'buzz-lightyear'})
        self.assertIn(new_event.id, self._get_scheduled_event_ids())

    def _populate_state(self, event_count=10):
        """
        Populate the state with events. No events are cancelled and all lecturers will have opted
        in.

        """
        events = [self._fake_event() for _ in range(event_count)]
        opt_ins = set()
        for e in events:
            e.cancelled = False
            for crsid in e.lecturer_crsids:
                opt_ins.add(crsid)

        self.state.opt_ins |= opt_ins
        self.state.location_to_agents.update(LOCATION_TO_AGENTS)
        self._update_state_events(events)

    def _update_state_events(self, events):
        self.state.events.update({e.id: e for e in events})

    def _update_random_event(self, **changes):
        """
        Update a random event with the changes passed. Return the new event. The event is selected
        from all events in the state, not just the scheduled ones.

        """
        e_id = random.choice(sorted(e.id for e in self.state.events.values()))
        new_event = dataclasses.replace(self.state.events[e_id], **changes)
        self._update_state_events([new_event])
        return new_event

    def _get_scheduled_event_ids(self):
        """
        Return a set of event ids which are currently in the schedule and are not revoked.

        """
        return {e.id for e, m in self.state.get_schedule() if not m.is_revoked}

    def _get_revoked_event_ids(self):
        """
        Return a set of event ids which are currently in the schedule and are revoked.

        """
        return {e.id for e, m in self.state.get_schedule() if m.is_revoked}

    def _fake_event(self):
        """
        Create a fake populated event.

        """
        return events.Event(
            id=uuid.uuid4().hex,
            cancelled=random.choice([True, False]),
            lecturer_crsids=random.sample(FAKE_CRSIDS, random.randint(0, len(FAKE_CRSIDS))),
            title=_random_string(),
            start_at=datetime.datetime(day=1, month=12, year=2016) + datetime.timedelta(
                seconds=random.randint(0, 60*60*24*265*10)
            ),
            duration=datetime.timedelta(seconds=random.randint(1, 60*60*3)),
            vle_urls=[_random_string()],
            sequence_id=random.choice(FAKE_SEQUENCES),
            sequence_index=random.randint(1, 10),
            location_id=random.choice(FAKE_LOCATIONS),
            series_id=random.choice(FAKE_SERIES)
        )
