import datetime
import json
import unittest

from dateutil.tz import tzutc

from .. import googlesheets

# Encode the valid event template as JSON to make doubly sure none of the tests below mutate it.
VALID_EVENT_TEMPLATE = json.dumps({
    'id': 'abc-123',
    'cancelled': '',
    'lecturer_crsids': 'spqr1',
    'start_at': '2006-01-02T15:04:05-07:00',
    'duration_minutes': '47',
    'title': 'Test event',
    'vle_urls': 'https://vle.invalid/course?id=45',
    'sequence_id': 'seq-xyz',
    'sequence_index': '1',
    'location_id': 'null_island',
    'series_id': 'botolphs-study'
})


class ParseSheetTestCase(unittest.TestCase):
    def setUp(self):
        # copy the headings list so that it may be safely mutated
        self.expected_headings = list(googlesheets.HEADINGS)

        # Create two valid events in a valid sheet
        self.rows = [googlesheets.HEADINGS]
        d = json.loads(VALID_EVENT_TEMPLATE)
        d['id'] = 'event-1'
        self.rows.append([d[k] for k in googlesheets.HEADINGS])
        d = json.loads(VALID_EVENT_TEMPLATE)
        d['id'] = 'event-2'
        d['start_at'] = '2006-01-02T15:04:05Z'
        self.rows.append([d[k] for k in googlesheets.HEADINGS])

    def test_empty_sheet(self):
        """An empty sheet should raise a ParseError."""
        with self.assertRaises(googlesheets.ParseError):
            googlesheets.parse_sheet([])

    def test_empty_sheet_with_headers(self):
        """An empty sheet but with expected headers should succeed."""
        googlesheets.parse_sheet([self.expected_headings])

    def test_empty_sheet_with_extra_headers(self):
        """An empty sheet but with expected headers and extra columns should succeed."""
        googlesheets.parse_sheet([self.expected_headings + ['foo', 'bar']])

    def test_empty_sheet_with_missing_headers(self):
        """An empty sheet but with a missing header should raise a parse error."""
        del self.expected_headings[1]
        del self.expected_headings[-2]
        with self.assertRaises(googlesheets.ParseError):
            googlesheets.parse_sheet([self.expected_headings])

    def test_basic_parse(self):
        """A simple valid sheet succeeds."""
        events = googlesheets.parse_sheet(self.rows)
        self.assertEqual(len(events), 2)

    def test_parse_error(self):
        """A sheet with a parse error in the first row still parses the second."""
        self.rows[1][googlesheets.HEADINGS.index('start_at')] = 'not-a-date'
        events = googlesheets.parse_sheet(self.rows)
        self.assertEqual(len(events), 1)


class ParseEventTestCase(unittest.TestCase):
    def setUp(self):
        # Create a valid event dict
        self.event_dict = json.loads(VALID_EVENT_TEMPLATE)

    def test_valid_event(self):
        """A valid event parses without error."""
        googlesheets.parse_event(self.event_dict)

    def test_id_parse(self):
        """Event id field is parsed correctly."""
        self.assert_event_parse('id', 'xyz-123')
        self.assert_event_parse('id', '', raises=True)
        self.assert_event_parse('id', '   xyz-123 ', expected_value='xyz-123')

    def test_start_at_parse(self):
        """Event start_at field is parsed correctly."""
        self.assert_event_parse(
            'start_at', '2006-01-02T15:04:05-07:00',
            datetime.datetime(2006, 1, 2, 22, 4, 5, tzinfo=tzutc())
        )

    def test_duration_minutes_parse(self):
        """Event duration_minutes field is parsed correctly."""
        def to_mins(n):
            return datetime.timedelta(minutes=n)
        self.assert_event_parse('duration_minutes', '123', to_mins(123), event_name='duration')
        self.assert_event_parse('duration_minutes', ' 123   ', to_mins(123), event_name='duration')
        self.assert_event_parse('duration_minutes', 'not-an-int', raises=True)
        self.assert_event_parse('duration_minutes', '0', raises=True)
        self.assert_event_parse('duration_minutes', '-10', raises=True)

    def test_lecturer_crsids_parse(self):
        """Event lecturer_crsids field is parsed correctly."""
        self.assert_event_parse('lecturer_crsids', '', [])
        self.assert_event_parse('lecturer_crsids', ',,,', [])
        self.assert_event_parse('lecturer_crsids', ' abc1 ', ['abc1'])
        self.assert_event_parse(
            'lecturer_crsids', ' abc1, spqr2, xyz3 ', ['abc1', 'spqr2', 'xyz3'])
        self.assert_event_parse('lecturer_crsids', ' abc1,, , xyz3 ', ['abc1', 'xyz3'])

    def test_title_parse(self):
        """Event title field is parsed correctly."""
        self.assert_event_parse('title', 'TESTING')
        self.assert_event_parse('title', '  TESTING     ', 'TESTING')

    def test_vle_urls_parse(self):
        """Event vle_url field is parsed correctly."""
        """Event lecturer_crsids field is parsed correctly."""
        self.assert_event_parse('vle_urls', '', [])
        self.assert_event_parse('vle_urls', ',,,', [])
        self.assert_event_parse(
            'vle_urls', ' http://vle.invalid/some/path ', ['http://vle.invalid/some/path']
        )
        self.assert_event_parse(
            'vle_urls',
            ' http://vle.invalid/some/path, http://vle.invalid/some/other/path ',
            ['http://vle.invalid/some/path', 'http://vle.invalid/some/other/path']
        )

    def test_sequence_id_parse(self):
        """Event sequence_id field is parsed correctly."""
        self.assert_event_parse('sequence_id', 'some-id')
        self.assert_event_parse('sequence_id', '', raises=True)
        self.assert_event_parse('sequence_id', '  some-id ', expected_value='some-id')

    def test_sequence_index_parse(self):
        """Event sequence_index field is parsed correctly."""
        self.assert_event_parse('sequence_index', '987', 987)
        self.assert_event_parse('sequence_index', '  987 ', 987)
        self.assert_event_parse('sequence_index', '0', 0)
        self.assert_event_parse('sequence_index', 'not-an-int', raises=True)
        self.assert_event_parse('sequence_index', '-10', raises=True)
        self.assert_event_parse('sequence_index', '', raises=True)

    def test_location_id_parse(self):
        """Event location_id field is parsed correctly."""
        self.assert_event_parse('location_id', 'some-loc-id')
        self.assert_event_parse('location_id', '  some-loc-id ', 'some-loc-id')
        self.assert_event_parse('location_id', '', raises=True)

    def test_series_id_parse(self):
        """Event series_id field is parsed correctly."""
        self.assert_event_parse('series_id', 'some-series-id')
        self.assert_event_parse('series_id', '  some-series-id ', 'some-series-id')
        self.assert_event_parse('series_id', '', raises=True)

    def test_required_fields(self):
        """Empty required fields cause a parse error."""
        self.assert_event_parse('start_at', '', raises=True)
        self.assert_event_parse('duration_minutes', '', raises=True)
        self.assert_event_parse('sequence_id', '', raises=True)
        self.assert_event_parse('sequence_index', '', raises=True)
        self.assert_event_parse('location_id', '', raises=True)
        self.assert_event_parse('series_id', '', raises=True)

    def test_cancelled_parse(self):
        """Event cancelled field is parsed correctly."""
        self.assert_event_parse('cancelled', '', expected_value=False)
        self.assert_event_parse('cancelled', ' ', expected_value=False)
        self.assert_event_parse('cancelled', 'Y', expected_value=True)
        self.assert_event_parse('cancelled', 'N', expected_value=False)
        self.assert_event_parse('cancelled', '  Y ', expected_value=True)
        self.assert_event_parse('cancelled', '  N ', expected_value=False)
        self.assert_event_parse('cancelled', '?', raises=True)

    def assert_event_parse(
            self, field_name, dict_value, expected_value=None, raises=False, event_name=None):
        expected_value = expected_value if expected_value is not None else dict_value
        event_name = event_name if event_name is not None else field_name
        self.event_dict[field_name] = dict_value
        if raises:
            with self.assertRaises(googlesheets.ParseError):
                googlesheets.parse_event(self.event_dict)
        else:
            event = googlesheets.parse_event(self.event_dict)
            self.assertEqual(getattr(event, event_name), expected_value)


class ConfigurationTestCase(unittest.TestCase):
    def setUp(self):
        self.valid_configuration = {
            'service_account_credentials_path': '/data/credentials.json',
            'keys': ['abcdefg', '12345678']
        }

    def test_basic_functionality(self):
        """A valid configuration is parsed."""
        config = googlesheets.Configuration.from_dict(self.valid_configuration)
        self.assertEqual(
            config.service_account_credentials_path,
            self.valid_configuration['service_account_credentials_path']
        )
        self.assertEqual(config.keys, self.valid_configuration['keys'])
        self.assertEqual(config.poll_delay_seconds, googlesheets.DEFAULT_POLL_DELAY)

    def test_missing_values(self):
        """Required keys are required."""
        for k in ['service_account_credentials_path', 'keys']:
            d = {}
            d.update(self.valid_configuration)
            del d[k]
            with self.assertRaises(ValueError):
                googlesheets.Configuration.from_dict(d)

    def test_poll_delay(self):
        """Overriding the poll delay is possible."""
        self.valid_configuration['poll_delay_seconds'] = googlesheets.DEFAULT_POLL_DELAY + 10.5
        config = googlesheets.Configuration.from_dict(self.valid_configuration)
        self.assertEqual(config.poll_delay_seconds, googlesheets.DEFAULT_POLL_DELAY + 10.5)

    def test_unknown_keys(self):
        """Unknown keys raise an exception."""
        d = {}
        d.update(self.valid_configuration)
        d['foo'] = 'bar'
        with self.assertRaises(ValueError):
            googlesheets.Configuration.from_dict(d)
