import asyncio
import dataclasses
import logging
import typing

from .events import Event, EventMetadata

LOG = logging.getLogger(__name__)


@dataclasses.dataclass
class StateUpdate:
    """
    A request to update the scheduler state which is put on the state update queue by event ingest
    loops.

    """
    #: A sequence of events which have changed.
    events: typing.Sequence[Event] = dataclasses.field(default_factory=list)

    #: Update the mapping of location ids to agent ids. This is authoritative; if non-None it will
    #: replace the existing mapping in its entirety,
    location_to_agents: typing.Optional[typing.Mapping[str, str]] = None

    #: Update the set of crsids of lecturers which have opted in to the service. This is
    #: authoritative; if non-None it will replace the existing set.
    opt_ins: typing.Optional[typing.Sequence[str]] = None


class State:
    """
    The state of the scheduler. Contains information on all events ingested, the current lecture
    opt-in/out state and the mapping from room locations to agent ids.

    """
    def __init__(self, *, handler_coros=[]):
        # List of coroutines which should be scheduled when state changes
        self._handler_coros = handler_coros

        # Create in-memory database of events keyed by id
        self.events = {}

        # Create in-memory database of crsids which have opt-ed in to lecture capture
        self.opt_ins = set()

        # Create in-memory database of mappings between location ids and agent names.
        self.location_to_agents = {}

        # Queue to send StateUpdate-s to
        self.queue = asyncio.Queue()

        # Schedule update queue listener
        asyncio.ensure_future(self._listen_for_updates())

    def get_schedule(self):
        """
        Return a sequence of (event, metadata) pairs which show which events should be scheduled at
        this moment in time. Events which should not be scheduled are filtered out of the list.

        """
        events_and_metadata = []

        for event in self.events.values():
            # Form event metadata from in-memory databases of agents
            metadata = EventMetadata(
                agent_id=self.location_to_agents.get(event.location_id),
                is_revoked=False)

            # Don't schedule this event if it is cancelled
            if event.cancelled:
                metadata.is_revoked = True

            # Don't schedule this event if we don't know the agent.
            if metadata.agent_id is None:
                LOG.warning(
                    'Skipping event "%s" as location "%s" could not be mapped to an agent id',
                    event.id, event.location_id
                )
                metadata.is_revoked = True

            # Don't schedule this event if any lecturer has not opted-in.
            if any(crsid not in self.opt_ins for crsid in event.lecturer_crsids):
                LOG.warning(
                    'Skipping event "%s" as not all of crsids %s have opted-in',
                    event.id, ', '.join(event.lecturer_crsids)
                )
                LOG.warning('Opt-in statuses: %s', ', '.join([
                    '{}: {}'.format(crsid, crsid in self.opt_ins)
                    for crsid in event.lecturer_crsids
                ]))
                metadata.is_revoked = True

            # This event has passwd all the tests, it can be scheduled.
            events_and_metadata.append((event, metadata))

        return events_and_metadata

    async def _listen_for_updates(self):
        """
        Co-routine which loops waiting for new updates on the update queue and updates the internal
        state accordingly. Calls state change handler co-routines.

        """
        LOG.info('Waiting for updates')

        while True:
            update = await self.queue.get()
            notify_handlers = False

            # Update state atomically. This state update is atomic since our concurrency is
            # co-routine based and so we know we will not be interrupted until the next await.

            # Form a new mapping from event id to events which will replace the self.events
            # state.
            new_events = {}
            new_events.update(self.events)
            new_events.update({event.id: event for event in update.events})

            # Optimisation: only trigger update handlers if there is any change in the events
            if self.events != new_events:
                LOG.info('Updating events')
                self.events = new_events
                notify_handlers = True
                LOG.info('Total number of events: %s', len(self.events))

            # Only update agent location state if it is present in update *and* it is different to
            # the current state.
            if (update.location_to_agents is not None
                    and self.location_to_agents != update.location_to_agents):
                LOG.info('Updating agent locations')
                # Take a shallow copy of the location -> agent map in case it is modified by
                # the event loop which passed it to us.
                self.location_to_agents = {}
                self.location_to_agents.update(update.location_to_agents)
                notify_handlers = True

            if update.opt_ins is not None:
                new_opt_ins = set(update.opt_ins)
                # Only update the opt in state if it is different to the current state.
                if new_opt_ins != self.opt_ins:
                    LOG.info('Updating lecturer opt-ins')
                    self.opt_ins = new_opt_ins
                    notify_handlers = True

            # Notify handlers of change in state if necessary.
            if notify_handlers:
                LOG.info('Notifying handlers of state change')
                await self._state_changed()
            else:
                LOG.info('No state change detected')

    async def _state_changed(self):
        """
        Coroutine. Call after changing internal state to notify state change handlers. The return
        value can be await-ed to wait for state change handlers to complete.

        """
        # TODO: if multiple co-routines start notifying about state change, this should be
        # re-factored to use something like asyncio.Condition.
        return await asyncio.gather(*[f(self) for f in self._handler_coros])
