"""
Main event loop for the scheduler.

"""
import asyncio
import logging
import sys

from . import googlesheets
from . import opencast
from . import preferences
from .state import State

LOG = logging.getLogger(__name__)


def run(configuration):
    # Run main loop(s)
    asyncio.run(main_loop(configuration))

    # The main loop should never terminate
    LOG.error('Unexpected exit from main loop')
    sys.exit(1)


async def main_loop(configuration):
    """
    Coroutine which runs all the main loops concurrently until they all exit.

    """
    handler_coros = []

    # Create an Opencast scheduler if configuration is present.
    if 'opencast' in configuration:
        opencast_scheduler = opencast.OpencastScheduler(
            config=opencast.Configuration.from_dict(configuration['opencast']))
        handler_coros.append(opencast_scheduler.update_state)

    # Initialise the scheduler state with a list of coroutines which should be scheduled when the
    # state changes.
    state = State(handler_coros=handler_coros)

    state.location_to_agents.update({
        item['location_id']: item['agent_id']
        for item in configuration.get('agent_locations', [])
    })

    # A list of co-routines which represent ingest loops. Each loop will ingest a set of events.
    # It then puts sequences of these events on the scheduler state update queue as StateUpdate
    # objects.
    ingest_loops = []

    # If there is configuration for the Google Sheets ingest, add the corresponding loop to the
    # list of ingest loops.
    if 'sheets' in configuration:
        ingest_loops.append(googlesheets.loop(
            queue=state.queue,
            configuration=googlesheets.Configuration.from_dict(configuration['sheets'])
        ))

    # If there is configuration for the Lecturer Preferences ingest, add the corresponding loop to
    # the list of ingest loops.
    if 'preferences' in configuration:
        ingest_loops.append(preferences.loop(
            queue=state.queue,
            configuration=preferences.Configuration.from_dict(configuration['preferences'])
        ))

    # Run long-lived ingest loops concurrently. Any one loop raising an exception will cause the
    # entire task to terminate with that exception. This ensures that things fail loud rather than
    # silently causing one of our task loops to be silently not running.
    return await asyncio.gather(*ingest_loops)
