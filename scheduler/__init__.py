"""
The Lecture Capture Events scheduler is a concurrent application written using the Python
`asyncio library <https://docs.python.org/3/library/asyncio.html>`_. It's job is to ingest Lecture
Capture events from a event sources, determine which events should be scheduled and schedule them
with the Opencast framework.

Events
------

.. automodule:: scheduler.events
    :members:
    :member-order: bysource

Google Sheets ingest
--------------------

.. automodule:: scheduler.googlesheets
    :members:
    :private-members:
    :member-order: bysource

Opencast scheduler
------------------

.. automodule:: scheduler.opencast
    :members:
    :member-order: bysource

Main loop
---------

.. automodule:: scheduler.loop
    :members:
    :member-order: bysource

.. automodule:: scheduler.state
    :members:
    :member-order: bysource

Utilities
---------

.. automodule:: scheduler.config
    :members:
    :member-order: bysource

"""
