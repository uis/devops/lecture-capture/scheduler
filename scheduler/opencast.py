"""
Opencast scheduling backend.

"""
import dataclasses
from datetime import datetime, timezone
import json
import logging
import re
import typing
import urllib.parse as urlparse

import aiohttp
from dateutil.parser import parse

from .config import ConfigurationDataclassMixin

LOG = logging.getLogger(__name__)


def _base_acl_factory():
    """Return the default base ACL."""
    return [
        {'role': 'ROLE_USER_ADMIN', 'action': 'read', 'allow': True},
        {'role': 'ROLE_USER_ADMIN', 'action': 'write', 'allow': True},
    ]


def _inputs_factory():
    """Return the default list of inputs."""
    return ['screen', 'AudioSource']


@dataclasses.dataclass
class Configuration(ConfigurationDataclassMixin):
    """
    Configuration for Opencast scheduler

    """
    #: Base URL for Opencast instance
    base_url: str

    #: Username of Opencast API user
    username: str

    #: Password of Opencast API user
    password: str

    #: Default workflow for scheduled events
    default_workflow: str

    #: Regex for matching moodle courses. The default will match courses on any Moodle site.
    moodle_url_regex: str = r'^https?://[^/]+/course/view\.php\?id=(?P<course_id>[0-9]+)$'

    #: Prefix used for encoding event ids in Opencast event subjects
    id_subject_prefix: str = 'ucam-event:'

    #: Special subject used to tag events managed by the scheduler
    subject_tag: str = 'ucam-scheduled-event'

    #: Base ACL
    base_acl: typing.Sequence[dict] = dataclasses.field(default_factory=_base_acl_factory)

    #: Default capture agent inputs for scheduled events
    default_inputs: typing.Sequence[str] = dataclasses.field(default_factory=_inputs_factory)

    #: Default series for scheduled events. (This is the UID of the series in Opencast.)
    default_series_id: str = ''


class OpencastScheduler:
    """
    Handle updates to the schedule and ensure that there are corresponding events scheduled in an
    Opencast instance.

    Scheduled events in the past are ignored.

    Revoked events are removed from the Opencast schedule. New events are added to the Opencast
    schedule and changes to event metadata cause the corresponding Opencast event to be changed.

    Note that events which disappear from the schedule (as opposed to being revoked) are ignored;
    they will not be removed from the Opencast schedule. This is because the schedule is
    authoritative; if an event does not appear in the schedule, we should not automate anything
    about it.

    """
    def __init__(self, config):
        self.config = config

        # Compile the moodle regex
        self.moodle_re = re.compile(self.config.moodle_url_regex)

    async def update_state(self, state):
        """
        Co-routine called when the local schedule state has changed.

        The current opencast events are retrieved and compared to the local events in the state and
        opencast events are created/updated/deleted such that they match the local events.

        """
        # Get the current schedule
        schedule = state.get_schedule()
        LOG.info('Events changed: %s in total, %s to schedule', len(state.events), len(schedule))

        # Fetch all scheduled opencast events and form a list of scheduled opencast events for each
        # event id.
        oc_events_by_event_id = {}
        for event in await self._fetch_oc_events():
            subjects = event.get('subjects', [])

            # Although events which are not managed by us should've been filtered, the Opencast API
            # is not capable of doing this for us.
            if self.config.subject_tag not in subjects:
                continue

            event_ids = [
                s[len(self.config.id_subject_prefix):]
                for s in subjects if s.startswith(self.config.id_subject_prefix)
            ]
            for event_id in event_ids:
                oc_events = oc_events_by_event_id.get(event_id, [])
                oc_events.append(event)
                oc_events_by_event_id[event_id] = oc_events

        LOG.info('Matched scheduled events: %s', len(oc_events_by_event_id))

        # Use a single "now" for deciding if an event is in the future
        now = datetime.now(timezone.utc)

        # Iterate over all events and create/update/delete them as appropriate.  If events
        # disappear from the schedule, we do not delete them. Rather we assume that they have been
        # "abandoned from automation". We only delete events which have been specifically revoked.
        # (I.e. cancelled, de-scheduled due to lecturer opt-out, etc.)
        for event, metadata in schedule:
            # Was this event in the past? If so, there's nothing we can do about it now
            if event.start_at < now:
                LOG.debug('Skipping event "%s" in the past', event.id)
                continue

            # Get the list of existing Opencast events
            oc_events = oc_events_by_event_id.get(event.id, [])

            LOG.debug('Existing Opencast events for event "%s":', event.id)
            _log_json(oc_events)

            if metadata.is_revoked:
                # Delete any Opencast events which exist for a revoked event
                for oc_event in oc_events:
                    LOG.info(
                        'Deleting Opencast event "%s" for matching event "%s" which is revoked',
                        oc_event['identifier'], event.id)
                    try:
                        await self._delete_oc_event(oc_event['identifier'])
                    except Exception as e:
                        LOG.error('Failed to delete event')
                        LOG.exception(e)
            elif len(oc_events) == 0:
                # There are no events in Opencast for this event and it is not revoked, create one
                LOG.info('Creating Opencast event for new event "%s"', event.id)
                try:
                    await self._ensure_event(event, metadata)
                except Exception as e:
                    LOG.error('Failed to create event')
                    LOG.exception(e)
            else:
                # Update the existing events if necessary.
                for oc_event in oc_events:
                    # Don't update events which are already correct.
                    if self._events_match(event, metadata, oc_event):
                        LOG.debug(
                            'Opencast event "%s" for event "%s" is already up to date',
                            oc_event['identifier'], event.id)
                        continue

                    LOG.info(
                        'Updating Opencast event "%s" for changed event "%s"',
                        oc_event['identifier'], event.id)

                    try:
                        await self._ensure_event(
                            event, metadata, identifier=oc_event['identifier'])
                    except Exception as e:
                        LOG.error('Failed to update event')
                        LOG.exception(e)

    async def _fetch_oc_events(self):
        """
        Fetch all the opencast events that are scheduled. The Opencast API does not let one filter
        for a partial subject match so the return value from this call should also be filtered by
        subject tag.

        Any errors will cause an exception to be raised.

        """
        return await self._api_get(
            endpoint='api/events/',
            params={
                'withacl': 'true',
                'withscheduling': 'true',
                'filter': 'status:EVENTS.EVENTS.STATUS.SCHEDULED',
            }
        )

    async def _ensure_event(self, event, metadata, identifier=None):
        """
        Create a new opencast event from the given source event.
        Any errors will cause an exception to be raised.a

        """
        payload = {
            'acl': self._create_acl_data(event),
            'metadata': self._metadata_dict(event),
            'scheduling': self._scheduling_dict(event, metadata),
            'processing': self._processing_dict(),
        }

        endpoint = '/api/events/' if identifier is None else f'/api/events/{identifier}'

        with aiohttp.MultipartWriter("form-data") as mp:
            for k, v in payload.items():
                part = mp.append_json(v)
                part.set_content_disposition('form-data', name=k)
            return await self._api_post(endpoint, data=mp)

    async def _delete_oc_event(self, event_id):
        """
        Delete an opencast event using the given event id.
        Any errors will cause an exception to be raised.

        """
        return await self._api_delete(f'/api/events/{event_id}')

    async def _api_get(self, endpoint, params=None):
        """
        GET a response from an API endpoint as a parsed JSON object. Get parameters are passed in
        the *params* dict. If the response has a failure status code an exception is raised.

        """
        async with aiohttp.ClientSession() as session:
            async with session.get(
                url=urlparse.urljoin(self.config.base_url, endpoint),
                auth=aiohttp.BasicAuth(self.config.username, self.config.password),
                params=params
            ) as response:
                if response.status >= 400:
                    LOG.error('Request failed with status %s', response.status)
                    LOG.debug('Response was: %r', await response.text())
                    raise RuntimeError(f'Unexpected status: {response.status}')
                return await response.json()

    async def _api_delete(self, endpoint):
        """
        DELETE a resource via the API. If the response has a failure status code an exception is
        raised.

        """
        async with aiohttp.ClientSession() as session:
            async with session.delete(
                url=urlparse.urljoin(self.config.base_url, endpoint),
                auth=aiohttp.BasicAuth(self.config.username, self.config.password),
            ) as response:
                if response.status >= 400:
                    LOG.error('Request failed with status %s', response.status)
                    LOG.debug('Response was: %r', await response.text())
                    raise RuntimeError(f'Unexpected status: {response.status}')
                return await response.text()

    async def _api_post(self, endpoint, data=None):
        async with aiohttp.ClientSession() as session:
            async with session.post(
                url=urlparse.urljoin(self.config.base_url, endpoint),
                auth=aiohttp.BasicAuth(self.config.username, self.config.password),
                data=data,
            ) as response:
                if response.status >= 400:
                    LOG.error('Request failed with status %s', response.status)
                    LOG.debug('Response was: %r', await response.text())
                    raise RuntimeError(f'Unexpected status: {response.status}')
                return await response.text()

    def _create_acl_data(self, event):
        """Create opencast ACL details for an event"""
        # Start with the base ACL
        acl = list(self.config.base_acl)

        # Match Moodle URLs and add Instructor and Learner ACLs for events.
        for vle_url in event.vle_urls:
            match = self.moodle_re.match(vle_url)
            if not match:
                continue

            course_id = match.group('course_id')
            acl.append({'role': f'{course_id}_Instructor', 'action': 'write', 'allow': True})
            acl.append({'role': f'{course_id}_Instructor', 'action': 'read', 'allow': True})
            acl.append({'role': f'{course_id}_Learner', 'action': 'read', 'allow': True})

        return acl

    def _events_match(self, event, metadata, oc_event):
        """
        Compares an event and opencast event. Returns True if and only if the events are equal.

        """
        if oc_event['title'] != event.title:
            return False
        scheduling = oc_event['scheduling']
        if _parse_date(scheduling['start']) != event.start_at:
            return False
        if _parse_date(scheduling['end']) != (event.start_at + event.duration):
            return False
        if scheduling['agent_id'] != metadata.agent_id:
            return False
        if oc_event['acl'] != self._create_acl_data(event):
            return False
        if oc_event['series'] != self.config.default_series_id:
            return False
        return True

    def _metadata_dict(self, event):
        """Return opencast metadata details for an event."""
        def _make_field(id_, value):
            return {'id': id_, 'value': value}

        return [
            {
                'flavor': 'dublincore/episode',
                'fields': [
                    _make_field('title', event.title),
                    _make_field('startDate', event.start_at.strftime('%Y-%m-%d')),
                    _make_field('startTime', event.start_at.strftime('%H:%M:%SZ')),
                    # note we use 'subjects' for makeshift tagging
                    _make_field('subjects', [
                        self.config.subject_tag,
                        f'{self.config.id_subject_prefix}{event.id}',
                    ]),
                    _make_field('isPartOf', self.config.default_series_id),
                ],
            }
        ]

    def _scheduling_dict(self, event, metadata):
        """Return opencast scheduling details for an event"""
        return {
            'agent_id': metadata.agent_id,
            'start': event.start_at.isoformat(),
            'end': (event.start_at + event.duration).isoformat(),
            'inputs': self.config.default_inputs,
        }

    def _processing_dict(self):
        """Return opencast processing details for an event"""
        return {
            'workflow': self.config.default_workflow,
            'configuration': {
                'flagForCutting': 'false',
                'flagForReview': 'false',
                'publishToEngage': 'true',
                'publishToHarvesting': 'true',
                'straightToPublishing': 'true'
            }
        }


def _parse_date(s):
    """Parse date from a date string retrieved from opencast"""
    return parse(s).astimezone(timezone.utc)


def _log_json(d):
    """Log a value as human readable JSON. The value is logged at the "debug" level."""
    for line in json.dumps(d, indent=2).splitlines():
        LOG.debug('%s', line)
