Lecture Capture Events Scheduler
================================

The Lecture Capture Events Scheduler ingests upcoming timetabled lectures for
the University and schedules them with an Opencast system.

.. toctree::
    :maxdepth: 2
    :caption: Contents

    gettingstarted
    hacking
    reference
