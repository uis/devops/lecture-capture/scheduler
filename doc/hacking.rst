Overview for Developers
=======================

The scheduler application is naturally concurrent in that it repeatedly
interacts with network APIs and has multiple sub-tasks running at the same time.
In order to simplify the concurrence, the scheduler uses the `asyncio module
<https://docs.python.org/3/library/asyncio.html>`_ from Python 3.

Main loop
---------

The main loop is found in :py:mod:`scheduler.loop`. Its job is to launch the
various ingest tasks, maintain a central scheduler state and to launch the
scheduler backend tasks when the event schedule changes.

The scheduler state is encapsulated within the
:py:class:`~scheduler.loop.SchedulerState` class. This class contains all the
logic to track which events have been ingested and which should actually be
scheduled. The current list of events which should be scheduled can be retrieved
via the :py:meth:`~scheduler.loop.SchedulerState.get_schedule` method.

The scheduler state is initialised with a list of backend coroutines. These
coroutines are launched whenever the list of events to schedule changes. They
are called with the scheduler state object as the first argument.

Ingest loops
------------

The main loop launches ingest loops as long-running tasks. Each ingest loop
receives an :py:class:`asyncio.Queue` object which they can use to inform the
scheduler of new or modified events. When new events are ingested, a **list** of
:py:class:`scheduler.events.Event` objects should be put on the queue. Events
which were previously posted and which should now be cancelled are represented
by setting a "cancelled" flag on the event.
