Getting Started
===============

Firstly, clone the repository:

.. code:: shell-session

    $ git clone git@tlab.developers.cam.ac.uk:uis/devops/lecture-capture/scheduler
    $ cd lecture-capture-scheduler

After cloning, you will need to create some initial configuration:

.. code:: shell-session

    $ mkdir .scheduler
    $ cp configuration-template.yaml .scheduler/configuration.yaml

The configuration template includes information on what configuration is
required:

.. literalinclude:: ../configuration-template.yaml
    :language: yaml

Google Sheets
-------------

In order to ingest data from Google Sheets, you will need to create a service
account and download JSON-formatted credentials for it. `Instructions on
creating a service account and credentials
<https://developers.google.com/identity/protocols/OAuth2ServiceAccount#creatinganaccount>`_
is available on Google's website. Place the credentials inside the
``.scheduler`` directory created above in a file named ``credentials.json``.

You will then need to create a Google Sheet with the correct schema. An example
CSV file with the correct schema is as follows:

.. literalinclude:: sheet-example.csv
    :language: csv

This sheet should be shared view-only with the email address of the service
account.

Add the Sheet keys for each Sheet to the ``keys`` section of the configuration.

Run the scheduler
-----------------

After completing the configuration, the scheduler may be run the
``scheduler_development.sh`` script:

.. code:: shell-session

    $ ./scheduler_development.sh
