# Lecture capture scheduling engine

This repository contains a tool to schedule lecture capture recordings. It has
the following features:

* Ingest events to be scheduled from Google Sheets.
* Respect lecturer opt-in/out preferences. (TODO)
* Reconciles Opencast events with the schedule. (TODO)
* Sets appropriate role-based access policies on Opencast events. (TODO)

## Further documentation

Usage and API documentation may be built using tox:

```bash
$ COMPOSE_ARGS="-v $PWD/build/:/tmp/tox-data/artefacts/" ./tox.sh -e doc
$ xdg-open build/doc/index.html   # Linux
$ open build/doc/index.html       # Mac
```

## Quickstart

> This quickstart is an abbreviated form of the getting started guide from the
> main documentation.

The ``./scheduler_development.sh`` script will build a containerised version of
the tool and run it with the repository directory mounted read-only under
``/usr/src/app`` inside the container. As such you can have development-local
configuration inside the repository.

When *first running* the tool, you will need to create some configuration. (See
the "Configuration" section in the documentation for what is required.)

```bash
$ cd /path/to/this/repo
$ mkdir .scheduler
$ cp configuration-template.yaml .scheduler/configuration.yaml
# ... edit configuration, see below ...
```

Once configured, the scheduler can be run as follows:

```bash
$ ./scheduler_development.sh
```

> The ``scheduler_development.sh`` script can be used to run the scheduler utility
> within a development Docker image. The repository directory is mounted read-only
> as ``/usr/src/app`` so that local modifications take effect.

## Running tests

The tests may be run using tox:

```bash
$ ./tox.sh
```
